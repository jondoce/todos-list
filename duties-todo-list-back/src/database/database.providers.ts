import * as mongoose from 'mongoose';

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: (): Promise<typeof mongoose> =>
        mongoose.connect('mongodb+srv://jdorado:ooI0r5oMfroXhzvT@cluster0.qz2jc.mongodb.net/DBAccounts?retryWrites=true&w=majority'),
  },
];
