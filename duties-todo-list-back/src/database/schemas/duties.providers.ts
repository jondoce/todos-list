import { Connection } from 'mongoose';
import { DutiesSchema } from './duties.schema';

export const dutiesProviders = [
  {
    provide: 'DUTY_MODEL',
    useFactory: (connection: Connection) => connection.model('Duty', DutiesSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
