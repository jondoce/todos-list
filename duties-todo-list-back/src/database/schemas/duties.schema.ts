import * as mongoose from 'mongoose';

export const DutiesSchema = new mongoose.Schema({
  id: String,
  name: String,
});
