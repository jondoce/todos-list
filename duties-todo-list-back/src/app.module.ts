import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { DutiesModule } from './duties/duties.module';

@Module({
  imports: [
    DatabaseModule,
    DutiesModule
  ]
})
export class AppModule {
}
