import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { dutiesProviders } from '../database/schemas/duties.providers';
import { DutiesController } from './controllers/duties.controller';
import { DutiesServices } from './services/duties.services';

@Module({
    imports: [DatabaseModule],
    controllers: [DutiesController],
    providers: [
        DutiesServices,
        ...dutiesProviders
    ]
})
export class DutiesModule {
}
