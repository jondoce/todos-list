import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { DutyDto } from '../dtos/duty-dto.model';
import { DutiesServices } from '../services/duties.services';

@Controller('duties')
export class DutiesController {

    constructor(private readonly dutiesServices: DutiesServices) {
    }

    @Get()
    async fetchDuties(): Promise<DutyDto[]> {
        return await this.dutiesServices.findAll();
    }

    @Get('duty/:dutyId')
    async findById(@Param('dutyId') dutyId: string): Promise<DutyDto> {
        return await this.dutiesServices.findOne(dutyId);
    }

    @Post('duty')
    async addDuty(@Body() dutyDto: DutyDto): Promise<DutyDto> {
        return await this.dutiesServices.create(dutyDto);
    }

    @Put('duty')
    putDuty(@Body() dutyDto: DutyDto): Promise<DutyDto> {
        return this.dutiesServices.update(dutyDto);
    }

}
