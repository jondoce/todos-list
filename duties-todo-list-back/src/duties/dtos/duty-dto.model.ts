export class DutyDto {

    constructor(
        public _id: string,
        public id: string,
        public name: string) {
    }

    // static fromModelToArray(duties: Duty[]) {
    //     return duties.map(d => new DutyDto(
    //       d.id,
    //       d.name)
    //     )
    // }

    // static fromModel(duty: Duty) {
    //     return new DutyDto(
    //       duty.id,
    //       duty.name
    //     )
    // }
}
