import { ConflictException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Model } from 'mongoose';
import { DutyDto } from '../dtos/duty-dto.model';

@Injectable()
export class DutiesServices {

    constructor(@Inject('DUTY_MODEL') private dutyModel: Model<DutyDto>) {
    }

    async findAll(): Promise<DutyDto[]> {
        return await this.dutyModel
            .find()
            .sort({'id': 1})
            .exec();
    }

    async findOne(dutyId: string): Promise<DutyDto> {
        const duty: DutyDto = await this.dutyModel
            .findById({_id: dutyId})
            .exec();

        if ( !duty ) {
            throw new NotFoundException(`Duty #${dutyId} not found`);
        }
        return duty;
    }

    async create(duty: DutyDto) {
        const existingDuty: DutyDto = await this.dutyModel
            .findOne({id: duty.id})
            .exec();

        if ( existingDuty ) {
            throw new ConflictException(`Duty #${duty.id} is duplicated.`);
        }

        const newDuty = await new this.dutyModel(duty);
        return newDuty.save();
    }

    async update(duty: DutyDto) {
        const existingDuty: DutyDto = await this.dutyModel
            .findByIdAndUpdate({_id: duty._id}, duty)
            .exec();

        if ( !existingDuty ) {
            throw new NotFoundException(`Duty #${duty.id} not found`);
        }

        return existingDuty;
    }
}
