# DutiesTodoListFront

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.4.

## Installation

Run `npm install`

## Development server

Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload
if you change any of the source files.

