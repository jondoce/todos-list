import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class UiFacilitiesService {

  constructor(private snackBar: MatSnackBar) {
  }

  showSnackBar(message: string, action: any, duration: number) {
    this.snackBar.open(message, action, {duration});
  }

  showMessage(text: string) {
    this.showSnackBar(text, null, 3000);
  }
}
