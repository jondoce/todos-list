import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-app-dialog',
  templateUrl: './app-dialog.component.html',
  styleUrls: ['./app-dialog.component.scss']
})
export class AppDialogComponent implements OnInit {

  @Input() title: string;
  @Input() subtitle: string;

  @Input() subtitleButtonText: string;

  @Input() backButtonText: string;
  @Input() leftButtonText: string;
  @Input() rightButtonText: string;
  @Input() alternativeButtonIcon: string;

  @Input() backButtonDisabled = false;
  @Input() leftButtonDisabled = false;
  @Input() rightButtonDisabled = false;
  @Input() rightButtonLoading = false;
  @Input() leftButtonLoading = false;

  @Output() subtitleAction: EventEmitter<any> = new EventEmitter<any>();
  @Output() leftButtonAction: EventEmitter<any> = new EventEmitter<any>();
  @Output() backButtonAction: EventEmitter<any> = new EventEmitter<any>();
  @Output() rightButtonAction: EventEmitter<any> = new EventEmitter<any>();
  @Output() closeButtonAction: EventEmitter<any> = new EventEmitter<any>();
  @Output() alternativeButtonAction: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
  }

  close() {
    this.closeButtonAction.emit();
  }

  subtitleButton() {
    this.subtitleAction.emit();
  }

  backButton() {
    this.backButtonAction.emit();
  }

  leftButton() {
    this.leftButtonAction.emit();
  }

  rightButton() {
    this.rightButtonAction.emit();
  }

  alternativeButton() {
    this.alternativeButtonAction.emit();
  }

}
