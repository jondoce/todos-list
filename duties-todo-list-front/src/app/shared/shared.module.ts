import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppDialogComponent } from './app-dialog/app-dialog.component';


@NgModule({
  declarations: [
    AppDialogComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    AppDialogComponent
  ]
})
export class SharedModule {
}
