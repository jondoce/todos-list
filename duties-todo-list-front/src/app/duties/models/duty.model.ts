export interface Duty {
  _id?: string;
  id: string;
  name: string;
}
