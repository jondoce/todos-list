export interface DisplayedColumnInfo {
  displayedText: string;
  value: string
}
