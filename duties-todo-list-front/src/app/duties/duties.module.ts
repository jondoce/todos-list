import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { DutiesCreationComponent } from './components/duties-creation/duties-creation.component';
import { DutiesDetailComponent } from './components/duties-detail/duties-detail.component';
import { DutiesListComponent } from './components/duties-list/duties-list.component';
import { DutiesComponent } from './components/duties/duties.component';
import { DutiesRoutingModule } from './duties-routing.module';

@NgModule({
  declarations: [
    DutiesComponent,
    DutiesListComponent,
    DutiesCreationComponent,
    DutiesDetailComponent
  ],
  imports: [
    DutiesRoutingModule,
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,

  ]
})
export class DutiesModule {
}
