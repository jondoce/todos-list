import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DutiesCreationComponent } from './components/duties-creation/duties-creation.component';
import { DutiesDetailComponent } from './components/duties-detail/duties-detail.component';
import { DutiesComponent } from './components/duties/duties.component';

const routes: Routes = [
  {
    path: '',
    component: DutiesComponent
  },
  {
    path: 'creation',
    component: DutiesCreationComponent
  },
  {
    path: 'detail/:dutyId',
    component: DutiesDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DutiesRoutingModule {
}
