import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Duty } from '../models/duty.model';

@Injectable({
  providedIn: 'root'
})
export class DutiesService {

  private readonly _api;

  constructor(private http: HttpClient) {
    this._api = environment.apiHost;
  }

  fetch(): Observable<Duty[]> {
    return this.http.get<Duty[]>(`${this._api}/duties`);
  }

  findById(id: string) {
    return this.http.get<Duty>(`${this._api}/duties/duty/${id}`);
  }

  create(newDuty: Duty) {
    return this.http.post<Duty>(`${this._api}/duties/duty`, newDuty);
  }

  update(updatedDuty: Duty) {
    return this.http.put<Duty>(`${this._api}/duties/duty`, updatedDuty);
  }
}
