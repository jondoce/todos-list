export const displayedColumns = () => {
  return [
    {
      displayedText: 'Id',
      value: 'id'
    },
    {
      displayedText: 'Name',
      value: 'name'
    }
  ];
};

export const columns = () => {
  return ['id', 'name'];
};
