import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { DisplayedColumnInfo } from '../../models/displayed-column-info.model';
import { Duty } from '../../models/duty.model';
import { DutiesService } from '../../services/duties.service';
import { columns, displayedColumns } from './duties.functions';

@Component({
  selector: 'app-duties-list',
  templateUrl: './duties-list.component.html',
  styleUrls: ['./duties-list.component.scss']
})
export class DutiesListComponent implements OnInit {

  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;

  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;

  // @ts-ignore
  dataSource: MatTableDataSource<Duty>;
  displayedColumns: DisplayedColumnInfo[] = displayedColumns();
  columns = columns();
  selectedId = '';
  noData = true;
  loading = false;

  constructor(private router: Router,
              private dutiesService: DutiesService
  ) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.dutiesService.fetch().subscribe(duties => {
        this.loading = false;
        if ( duties ) {
          this.dataSource = new MatTableDataSource(duties);
          this.noData = !this.dataSource || this.dataSource.data.length === 0;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      error => this.loading = false);
  }

  detail(row: Duty) {
    return this.router.navigate(['detail', row._id]);
  }

  create() {
    return this.router.navigate(['creation']);
  }
}
