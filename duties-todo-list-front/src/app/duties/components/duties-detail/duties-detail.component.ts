import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiFacilitiesService } from '../../../core/services/ui-facilities.service';
import { Duty } from '../../models/duty.model';
import { DutiesService } from '../../services/duties.service';

@Component({
  selector: 'app-duties-detail',
  templateUrl: './duties-detail.component.html',
  styleUrls: ['./duties-detail.component.scss']
})
export class DutiesDetailComponent implements OnInit, OnDestroy {

  // @ts-ignore
  private dSubs: Subscription;

  // @ts-ignore
  private dutyId: string;

  // @ts-ignore
  private duty: Duty;

  // @ts-ignore
  dutyForm: FormGroup = null;
  loading = false;

  constructor(private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private dutiesService: DutiesService,
              private uiFacilitiesService: UiFacilitiesService) {
  }

  get id() {
    return this.dutyForm.controls.id;
  }

  get name() {
    return this.dutyForm.controls.name;
  }

  ngOnInit(): void {
    this.loading = true;
    this.dutyId = this.route.snapshot.params.dutyId;
    this.dutiesService.findById(this.dutyId).subscribe(duty => {
      if ( duty ) {
        this.duty = duty;
        this.dutyForm = this.fb.group({
          id: [duty.id, [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(10)
          ]],
          name: [duty.name, [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(250)
          ]]
        });
        this.loading = false;
      }
    });

  }

  ngOnDestroy(): void {
    if ( this.dSubs ) {
      this.dSubs.unsubscribe();
    }
  }

  onSubmit() {
    this.loading = true;
    if ( !this.dutyForm.valid ) {
      return;
    }

    const {id, name} = this.dutyForm.value;

    const updatedDuty: Duty = {
      _id: this.duty._id,
      id,
      name
    };
    this.dSubs = this.dutiesService.update(updatedDuty).subscribe(response => {
      if ( response ) {
        this.uiFacilitiesService.showMessage('Selected duty has been successfully updated.');
        this.dutyForm.reset();
        this.router.navigateByUrl('');
      }
      this.loading = false;
    }, error => {

      this.loading = false;
      this.uiFacilitiesService.showMessage('An error has been found.');
    });

  }
}
