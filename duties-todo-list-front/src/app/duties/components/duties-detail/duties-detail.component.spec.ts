import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DutiesDetailComponent } from './duties-detail.component';

describe('DutiesDetailComponent', () => {
  let component: DutiesDetailComponent;
  let fixture: ComponentFixture<DutiesDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DutiesDetailComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DutiesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
