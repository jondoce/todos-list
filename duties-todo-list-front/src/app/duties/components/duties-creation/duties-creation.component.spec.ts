import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DutiesCreationComponent } from './duties-creation.component';

describe('DutiesCreationComponent', () => {
  let component: DutiesCreationComponent;
  let fixture: ComponentFixture<DutiesCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DutiesCreationComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DutiesCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
