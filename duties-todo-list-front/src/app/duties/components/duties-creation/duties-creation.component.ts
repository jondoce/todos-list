import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiFacilitiesService } from '../../../core/services/ui-facilities.service';
import { Duty } from '../../models/duty.model';
import { DutiesService } from '../../services/duties.service';

@Component({
  selector: 'app-duties-creation',
  templateUrl: './duties-creation.component.html',
  styleUrls: ['./duties-creation.component.scss']
})
export class DutiesCreationComponent implements OnInit, OnDestroy {

  // @ts-ignore
  private dSubs: Subscription;

  // @ts-ignore
  dutyForm: FormGroup = null;
  loading = false;

  constructor(private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private dutiesService: DutiesService,
              private uiFacilitiesService: UiFacilitiesService) {
  }

  get id() {
    return this.dutyForm.controls.id;
  }

  get name() {
    return this.dutyForm.controls.name;
  }

  ngOnInit(): void {
    this.dutyForm = this.fb.group({
      id: ['', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(10)
      ]
      ],
      name: ['', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50)
      ]]
    });
  }

  ngOnDestroy(): void {
    if ( this.dSubs ) {
      this.dSubs.unsubscribe();
    }
  }

  onSubmit(): void {
    this.loading = true;
    if ( !this.dutyForm.valid ) {
      return;
    }

    const {id, name} = this.dutyForm.value;

    const newDuty: Duty = {
      id,
      name
    };
    this.dSubs = this.dutiesService.create(newDuty).subscribe(response => {
      if ( response ) {
        this.uiFacilitiesService.showMessage('A new duty has been successfully created.');
        this.dutyForm.reset();
        this.router.navigateByUrl('');
      }
      this.loading = false;
    }, error => {
      this.loading = false;
      this.uiFacilitiesService.showMessage(error.error.message);
    });

  }

}
